package KossyTest::Web;

use strict;
use warnings;
use utf8;
use Kossy;

use DBI;

sub mylog {
    my @str = @_;
    my $logfile = ">> /home/shioshiota/app/cpanminus/KossyTest/log.txt";

    open(LOG, $logfile);
    print(LOG join("\n", @_));
    close(LOG);

    return 1;
}



filter 'set_title' => sub {
    my $app = shift;
    sub {
        my ( $self, $c )  = @_;
        $c->stash->{site_name} = __PACKAGE__;
        $app->($self,$c);
    }
};

get '/' => [qw/set_title/] => sub {
    my ( $self, $c )  = @_;
    my $rows = entry_list(($self));
    my $nametable = get_nametable($self);
    $c->render('index.tx', { entries => $rows, nametable => $nametable});
};

get '/add' => [qw/set_title/] => sub {
    my ( $self, $c )  = @_;
    my $rows = member_list($self);
    my @entries;
    my $i=0;
    foreach my $r (@$rows){
	my ($id, $name) = @$r;
	$entries[$i++] = {id => $id, 
			name => $name};
    }
    $c->render('add.tx', {addType => "add", entries=>\@entries});
};


get '/search' => [qw/set_title/] => sub {
    my ( $self, $c )  = @_;
    my $rows = member_list($self);
    my @entries;
    my $i=0;
    foreach my $r (@$rows){
	my ($id, $name) = @$r;
	$entries[$i++] = {id => $id, 
			name => $name};
    }
    $c->render('search.tx', {entries=>\@entries});
};



sub query{
    my ( $self, $c )  = @_;
    my $checkList = get_check_list($self, $c);
    
    my $checkedMap = {};
    
    foreach my $r (@$checkList){
	if($r->{flag} eq "on"){
	    $checkedMap->{$r->{id}} = 1;
	}else {
	    $checkedMap->{$r->{id}} = 0;
	}
    }
    
    my $dbh = $self->dbh([$self]);
    my $s = "SELECT * FROM Todos;";
    my $sth = $dbh->prepare($s);
    $sth ->execute;
    my $rows= $sth->fetchall_arrayref;
    $sth->finish;
    $dbh->disconnect;
    
    
    my $ret = {};
    
    foreach my $r (@$rows){
	my $numOfMembers = 0;
	my @rr = @$r;
	for(my $i = 4; $i<=$#rr; $i++){
	    if($checkedMap->{$rr[$i]}){
		$numOfMembers++;
	    }
	}
	my %hash = %$ret;
	my @arr;
	if(exists($hash{$numOfMembers})){
	    my $p = $ret->{$numOfMembers};
	    @arr = @$p;
	}
	push @arr, $r;
	$ret->{$numOfMembers} =  \@arr;
    }
    return $ret;
}

post '/query' => sub{
    my ( $self, $c )  = @_;
    my $result = query(@_);
    my @nums = sort keys %$result;
    @nums = reverse(@nums);
    $c->render('result.tx', { entries => $result, nums=>\@nums});
};




#textbox1
get '/textbox' => sub {
    my ( $self, $c )  = @_;
    $c->render('textbox.tx', { greeting => "Hello" });
};

sub dbh{
    my $self = shift;
    my $database = 'DBI:mysql:KossyTest';
    my $dbuser = 'root';
    my $pass = 'dbpass';
    DBI->connect($database, $dbuser, $pass);
    return DBI->connect($database, $dbuser, $pass);
};


sub add_data{
    my $self = shift;
    my ($name, $deadline, $body, $addType, $id, $checkref) = @_;
    my $s;
    if($addType eq "add"){
	$s = "INSERT INTO Todos VALUES(0, \"$deadline\", \"$name\", \"$body\" ";
	foreach my $r (@$checkref){
	    if($r->{flag} eq "on"){
		$s = "$s , $r->{id} ";
	    }else{
		$s = "$s , 0 ";
	    }
	}
	$s = "$s );";
    }
    else {
	$s = "UPDATE Todos SET ".
	    "deadline = \"$deadline\",".
	    "name = \"$name\",".
	    "todo = \"$body\"".
	    " WHERE id=$id ;";
    }
    
    my $dbh = $self->dbh([$self]);
    my $sth = $dbh->prepare($s);

    $sth = $dbh->prepare($s);
    $sth ->execute;
    $sth->finish;
    $dbh->disconnect;
};


sub delete_data{
    my $self = shift;
    my ($id) = @_;
    my $dbh = $self->dbh([$self]);
    my $s = "DELETE FROM Todos WHERE id = $id;";
    my $sth = $dbh->prepare($s);
    $sth ->execute;
    $sth->finish;
    $dbh->disconnect;
};


sub get_nametable{
    my $self = shift;
    my $nameTable = {};
    my $memlist = member_list($self);
    foreach my $mem (@$memlist){
	my ($id, $name) = @$mem;
	$nameTable->{$id} = $name;
    }
    return $nameTable;
}

sub entry_list{
    my $self = shift;
    my $dbh = $self->dbh([$self]);
    my $s = "SELECT * FROM Todos;";
    my $sth = $dbh->prepare($s);
    $sth ->execute;
    my $rows= $sth->fetchall_arrayref;
    $sth->finish;
    $dbh->disconnect;
    
    my $memberList = member_list($self);

    my $nameMap ={};
    
    foreach my $r (@$memberList){
	my ($id, $name) = @$r;
	$nameMap->{$id} = $name;
    }

    foreach my $r (@$rows){
	my @rr = @$r;
	
	my @mem;
	
	for(my $i = 4; $i<=$#rr; $i++){
	    if($rr[$i]){
		my $hoge = $nameMap->{$rr[$i]};
		push @mem ,$hoge;
	    }
	}
	$rr[4] = \@mem;
	$r = \@rr;
    }
    return $rows;
};

sub member_list{
    my $self = shift;
    my $dbh = $self->dbh([$self]);
    my $s = "SELECT * FROM Members order by id;";
    my $sth = $dbh->prepare($s);
    $sth ->execute;
    my $rows= $sth->fetchall_arrayref;
    $sth->finish;
    $dbh->disconnect;
    return $rows;
};



sub get_entry_by_id{
    my $self = shift;
    my ($id) = @_;
    my $dbh = $self->dbh([$self]);
    my $s = "SELECT * FROM Todos WHERE id=$id;";
    my $sth = $dbh->prepare($s);
    $sth ->execute;
    my @row= $sth->fetchrow_array;
    $sth->finish;
    $dbh->disconnect;
    return @row;
};




sub get_check_list{
    my ( $self, $c )  = @_;
    my $rows = member_list($self);
    
    my @checkList;
    
    foreach my $r (@$rows){
	my ($id, $name) = @$r;
	push @checkList  ,{id=>"$id", flag=>$c->req->param("member$id")};
    }
    return \@checkList;
}

post '/putdata' => sub{
    my ( $self, $c )  = @_;
    my @data = ($c->req->param("name"), 
		$c->req->param("deadline"), 
		$c->req->param("body"),
		$c->req->param("addType"),
		$c->req->param("id")
	);
    
    my $checkList = get_check_list($self, $c);
    
    add_data($self, @data, $checkList);
    
    my $rows = entry_list(($self));
    my $nametable = get_nametable($self);
    $c->render('index.tx', { entries => $rows, nametable => $nametable});
};

post '/delete' => sub{
    my ( $self, $c )  = @_;
    my $id = $c->req->param("id");
    
    delete_data($self, $id);
    
    my $rows = entry_list(($self));
    $c->render('index.tx', { entries => $rows});
};


sub delete_member{
    my $self = shift;
    my ($id) = @_;
    my $dbh = $self->dbh([$self]);
    my $s = "DELETE FROM Members WHERE id = $id;";
    my $sth = $dbh->prepare($s);
    $sth ->execute;



    $s = "ALTER TABLE Todos DROP member$id;";
        
    $sth = $dbh->prepare($s);
    $sth ->execute;
    

    $sth->finish;
    $dbh->disconnect;
};


post '/deleteMember' => sub{
    my ( $self, $c )  = @_;
    my $id = $c->req->param("id");
    delete_member($self, $id);
    my $rows = member_list($self);
    $c->render('memberIndex.tx', {entries => $rows});
};


post '/edit' => sub{
    my ( $self, $c )  = @_;
    my $id = $c->req->param("id");
    my @row = get_entry_by_id($self, $id);
    shift @row;
    my ($deadline, $name, $body) = @row;
    $c->render('add.tx', { id=> $id, 
			   deadline => $deadline,
			   name => $name,
			   body => $body,
			   addType => "edit"
	       });
};






sub add_member{
    my $self = shift;
    my ($name) = @_;
    my $dbh = $self->dbh([$self]);
    my $s = "INSERT INTO Members VALUES(0, \"$name\");";
    my $sth = $dbh->prepare($s);
    $sth ->execute;
    $sth->finish;
    
    $s = "select MAX(id) from Members;";
    $sth = $dbh->prepare($s);
    $sth ->execute;
    my @r = $sth->fetchrow_array;
    my $id = $r[0];
    $sth->finish;
    
    $s = "ALTER TABLE Todos ADD member$id INT;";
        
    $sth = $dbh->prepare($s);
    $sth ->execute;
    
    $sth->finish;
    $dbh->disconnect;
};




post '/putMember' => sub{
    my ( $self, $c )  = @_;
    my $name = $c->req->param("name");
    add_member($self, $name);
    my $rows = member_list($self);
    $c->render('memberIndex.tx', {entries => $rows});
};

get '/addMember' => sub{
    my ( $self, $c )  = @_;
    $c->render('addMember.tx');
};

get '/memberIndex' => sub{
    my ( $self, $c )  = @_;
    my $rows = member_list($self);
    $c->render('memberIndex.tx', {entries => $rows});
};



1;


